 #!/bin/bash

code-server \
    --bind-addr 0.0.0.0:4242 \
    --user-data-dir /vscode/data \
    --extensions-dir /vscode/extensions \
    --disable-telemetry \
    --auth "none" \
    "$@"
